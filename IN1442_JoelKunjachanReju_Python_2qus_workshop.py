{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "9dc2e16b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "  PRODUCT_TYPE  FEB 1, 2019  FEB 2, 2019  FEB 3, 2019  FEB 4, 2019  \\\n",
      "0        Games            0            0            0            0   \n",
      "1  Electronics           10            0            0            0   \n",
      "2      Gadgets            0            0            0           30   \n",
      "\n",
      "   FEB 5, 2019  FEB 6, 2019  FEB 7, 2019  FEB 8, 2019  FEB 9, 2019  ...  \\\n",
      "0            0            0            0            0            0  ...   \n",
      "1            0            0            0            0            0  ...   \n",
      "2            0            0           50            0            0  ...   \n",
      "\n",
      "   FEB 19, 2021  FEB 20, 2021  FEB 21, 2021  FEB 22, 2021  FEB 23, 2021  \\\n",
      "0           237           227           235           261           197   \n",
      "1            84            97            96           123            80   \n",
      "2            61            52            63            95            82   \n",
      "\n",
      "   FEB 24, 2021  FEB 25, 2021  FEB 26, 2021  FEB 27, 2021  FEB 28, 2021  \n",
      "0           265           285           245           247           227  \n",
      "1           100            96            78            84            91  \n",
      "2            90            78            84            84            92  \n",
      "\n",
      "[3 rows x 760 columns]\n"
     ]
    }
   ],
   "source": [
    "'''2)\tWrite a python script to transform the data available in the Data Files using Python/Pandas to be able to fit into a typical Database table. Aggregate the sales of both the branches and load the cleansed and transformed data into a CSV file (final.csv) that can be easily loaded into a table.\n",
    "\n",
    "Come up with the table DDL with all the necessary fields/columns needed. (Refer to Data Files: Branch1_Sales.csv, Branch2_Sales.csv; Files contain sales information of two different Convenient store branches; As of now, data is only made available until Feb 2021)\n",
    "'''\n",
    "\n",
    "import pandas as pd\n",
    "sales1=pd.read_csv(\"C:\\\\Users\\\\GAndaver\\\\Downloads\\\\Branch1_sales.csv\")\n",
    "df = pd.DataFrame(sales1)\n",
    "df1=df.rename(index={\"GAMES\":\"Games\"},inplace =False)\n",
    "print(df1)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "04f88f9d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "    PRODUCT_TYPE  FEB 1, 2019  FEB 2, 2019  FEB 3, 2019  FEB 4, 2019  \\\n",
      "0          GAMES           10          990            0            0   \n",
      "1          MUSIC            0            0            0            0   \n",
      "2  FITNESS GOODS         1230          102          400          120   \n",
      "\n",
      "   FEB 5, 2019  FEB 6, 2019  FEB 7, 2019  FEB 8, 2019  FEB 9, 2019  ...  \\\n",
      "0            0            0            0            0          110  ...   \n",
      "1          140            0            0            0            0  ...   \n",
      "2            0            0            0            0            0  ...   \n",
      "\n",
      "   FEB 19, 2021  FEB 20, 2021  FEB 21, 2021  FEB 22, 2021  FEB 23, 2021  \\\n",
      "0           237           227           235           261           197   \n",
      "1            84            97            96           123            80   \n",
      "2            61            52            63            95            82   \n",
      "\n",
      "   FEB 24, 2021  FEB 25, 2021  FEB 26, 2021  FEB 27, 2021  FEB 28, 2021  \n",
      "0           265           285           245           247           227  \n",
      "1           100            96            78            84            91  \n",
      "2            90            78            84            84            92  \n",
      "\n",
      "[3 rows x 760 columns]\n"
     ]
    }
   ],
   "source": [
    "sales2=pd.read_csv(\"C:\\\\Users\\\\GAndaver\\\\Downloads\\\\Branch2_sales.csv\")\n",
    "df2 = pd.DataFrame(sales2)\n",
    "print(df2)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "e9ccb608",
   "metadata": {},
   "outputs": [],
   "source": [
    "file = pd.concat([df1,df2])\n",
    "types=file.groupby(\"PRODUCT_TYPE\", sort= False).sum()\n",
    "types1=types.unstack()\n",
    "name=types1.reset_index()\n",
    "name.columns=['SOLD_DATE','PRODUCT_TYPE','QTY_SOLD']\n",
    "name\n",
    "\n",
    "name.to_csv(\"final.csv\")\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3b803806",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
