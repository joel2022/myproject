{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "2939b541",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "    mag                                       place           time  \\\n",
      "0   5.5            7 km SW of Heunghae, South Korea  1510723772820   \n",
      "1   6.5              18 km W of Parrita, Costa Rica  1510540103680   \n",
      "2   6.1                 7 km NNE of Ixtepec, Mexico  1506171181480   \n",
      "3   5.1                29 km ENE of Koysinceq, Iraq  1503495773880   \n",
      "4   6.5                    168 km SW of Mawu, China  1502198389540   \n",
      "5   6.6                    11 km ENE of Kos, Greece  1500589871260   \n",
      "6   6.5               2 km S of Lim-oo, Philippines  1499328237550   \n",
      "7   6.8      28 km SW of Puerto San José, Guatemala  1498134663490   \n",
      "8   6.9            2 km SSW of San Pablo, Guatemala  1497425344390   \n",
      "9   6.3                   5 km S of Plomári, Greece  1497270519150   \n",
      "10  5.6                  34 km NNW of Bojn?rd, Iran  1494698459520   \n",
      "11  5.4           131 km ESE of Murghob, Tajikistan  1494453501700   \n",
      "12  6.9                40 km W of Valparaíso, Chile  1493069910820   \n",
      "13  6.1             61 km NNW of Torbat-e J?m, Iran  1491372552200   \n",
      "14  5.2       7 km SSE of Stilfontein, South Africa  1491188931210   \n",
      "15  6.6  81 km NNE of Ust’-Kamchatsk Staryy, Russia  1490760564190   \n",
      "16  5.6                   5 km NW of Samsat, Turkey  1488452846880   \n",
      "17  6.5               11 km N of Mabua, Philippines  1486735423920   \n",
      "18  7.9      35 km WNW of Panguna, Papua New Guinea  1485059422960   \n",
      "19  5.6                4 km SE of Cittareale, Italy  1484735125490   \n",
      "\n",
      "          updated    tz                                                url  \\\n",
      "0   1642438297892  None  https://earthquake.usgs.gov/earthquakes/eventp...   \n",
      "1   1597454571287  None  https://earthquake.usgs.gov/earthquakes/eventp...   \n",
      "2   1625720108635  None  https://earthquake.usgs.gov/earthquakes/eventp...   \n",
      "3   1511322209040  None  https://earthquake.usgs.gov/earthquakes/eventp...   \n",
      "4   1594399705896  None  https://earthquake.usgs.gov/earthquakes/eventp...   \n",
      "5   1626726853838  None  https://earthquake.usgs.gov/earthquakes/eventp...   \n",
      "6   1594395288392  None  https://earthquake.usgs.gov/earthquakes/eventp...   \n",
      "7   1594399480407  None  https://earthquake.usgs.gov/earthquakes/eventp...   \n",
      "8   1625271218365  None  https://earthquake.usgs.gov/earthquakes/eventp...   \n",
      "9   1594399436036  None  https://earthquake.usgs.gov/earthquakes/eventp...   \n",
      "10  1594395104454  None  https://earthquake.usgs.gov/earthquakes/eventp...   \n",
      "11  1594395081633  None  https://earthquake.usgs.gov/earthquakes/eventp...   \n",
      "12  1646013135561  None  https://earthquake.usgs.gov/earthquakes/eventp...   \n",
      "13  1609713244880  None  https://earthquake.usgs.gov/earthquakes/eventp...   \n",
      "14  1594394689207  None  https://earthquake.usgs.gov/earthquakes/eventp...   \n",
      "15  1594399370877  None  https://earthquake.usgs.gov/earthquakes/eventp...   \n",
      "16  1594394602087  None  https://earthquake.usgs.gov/earthquakes/eventp...   \n",
      "17  1620282777388  None  https://earthquake.usgs.gov/earthquakes/eventp...   \n",
      "18  1594394562046  None  https://earthquake.usgs.gov/earthquakes/eventp...   \n",
      "19  1594394522903  None  https://earthquake.usgs.gov/earthquakes/eventp...   \n",
      "\n",
      "                                               detail  felt  cdi    mmi  ...  \\\n",
      "0   https://earthquake.usgs.gov/fdsnws/event/1/que...   365  7.7  6.772  ...   \n",
      "1   https://earthquake.usgs.gov/fdsnws/event/1/que...   645  7.8  7.521  ...   \n",
      "2   https://earthquake.usgs.gov/fdsnws/event/1/que...   205  6.7  6.372  ...   \n",
      "3   https://earthquake.usgs.gov/fdsnws/event/1/que...    40  5.5  7.140  ...   \n",
      "4   https://earthquake.usgs.gov/fdsnws/event/1/que...   131  5.4  7.417  ...   \n",
      "5   https://earthquake.usgs.gov/fdsnws/event/1/que...   562  7.0  6.728  ...   \n",
      "6   https://earthquake.usgs.gov/fdsnws/event/1/que...   104  8.7  7.606  ...   \n",
      "7   https://earthquake.usgs.gov/fdsnws/event/1/que...   512  6.3  5.952  ...   \n",
      "8   https://earthquake.usgs.gov/fdsnws/event/1/que...   584  7.8  6.468  ...   \n",
      "9   https://earthquake.usgs.gov/fdsnws/event/1/que...   272  9.1  8.370  ...   \n",
      "10  https://earthquake.usgs.gov/fdsnws/event/1/que...    62  6.9  7.730  ...   \n",
      "11  https://earthquake.usgs.gov/fdsnws/event/1/que...     2  7.8  6.576  ...   \n",
      "12  https://earthquake.usgs.gov/fdsnws/event/1/que...  1117  9.1  6.662  ...   \n",
      "13  https://earthquake.usgs.gov/fdsnws/event/1/que...   249  7.3  7.415  ...   \n",
      "14  https://earthquake.usgs.gov/fdsnws/event/1/que...   119  6.8  6.903  ...   \n",
      "15  https://earthquake.usgs.gov/fdsnws/event/1/que...     1  6.1  6.664  ...   \n",
      "16  https://earthquake.usgs.gov/fdsnws/event/1/que...    13  7.8  7.154  ...   \n",
      "17  https://earthquake.usgs.gov/fdsnws/event/1/que...    93  8.1  7.456  ...   \n",
      "18  https://earthquake.usgs.gov/fdsnws/event/1/que...    30  9.1  7.253  ...   \n",
      "19  https://earthquake.usgs.gov/fdsnws/event/1/que...    23  4.8  6.511  ...   \n",
      "\n",
      "                                   ids     sources  \\\n",
      "0                         ,us2000bnrs,        ,us,   \n",
      "1   ,at00ozc47b,pt17317000,us2000bmhe,  ,at,pt,us,   \n",
      "2                         ,us2000atjv,        ,us,   \n",
      "3                         ,us2000aay1,        ,us,   \n",
      "4                         ,us2000a5x1,        ,us,   \n",
      "5                         ,us20009ynd,        ,us,   \n",
      "6   ,us1000976a,pt17187001,at00osnt2p,  ,us,pt,at,   \n",
      "7   ,us20009p1a,pt17173000,at00ory83s,  ,us,pt,at,   \n",
      "8   ,us20009mbt,pt17165003,at00orj0si,  ,us,pt,at,   \n",
      "9                         ,us20009ly0,        ,us,   \n",
      "10                        ,us10008s7b,        ,us,   \n",
      "11                        ,us10008rah,        ,us,   \n",
      "12  ,us10008kce,at00ooxo40,pt17114000,  ,us,at,pt,   \n",
      "13                        ,us10008ei0,        ,us,   \n",
      "14                        ,us10008e0f,        ,us,   \n",
      "15  ,us20008vhl,pt17088050,at00onk67q,  ,us,pt,at,   \n",
      "16                        ,us100086gw,        ,us,   \n",
      "17  ,us20008ixa,pt17041050,at00ol5web,  ,us,pt,at,   \n",
      "18  ,us10007uph,at00ok5z6p,pt17022050,  ,us,at,pt,   \n",
      "19                        ,us10007twn,        ,us,   \n",
      "\n",
      "                                                types   nst    dmin   rms gap  \\\n",
      "0   ,dyfi,losspager,moment-tensor,origin,phase-dat...  None   1.545  0.71  26   \n",
      "1   ,dyfi,ground-failure,impact-link,impact-text,l...  None   0.225  1.25  37   \n",
      "2   ,dyfi,impact-text,losspager,moment-tensor,orig...  None   1.306  1.32  31   \n",
      "3   ,dyfi,losspager,moment-tensor,origin,phase-dat...  None   0.700  0.80  43   \n",
      "4   ,dyfi,general-text,impact-text,losspager,momen...  None  11.411  0.77  22   \n",
      "5   ,dyfi,general-text,impact-text,losspager,momen...  None   0.913  0.78  16   \n",
      "6   ,dyfi,impact-link,impact-text,losspager,moment...  None   4.139  0.65  30   \n",
      "7   ,dyfi,general-text,impact-link,losspager,momen...  None   1.065  1.46  38   \n",
      "8   ,dyfi,general-text,impact-link,impact-text,los...  None   0.247  1.42  35   \n",
      "9   ,dyfi,general-text,impact-text,losspager,momen...  None   1.770  0.76  55   \n",
      "10  ,dyfi,impact-text,losspager,moment-tensor,orig...  None   0.738  1.12  36   \n",
      "11  ,dyfi,impact-text,losspager,moment-tensor,orig...  None   2.901  0.92  31   \n",
      "12  ,dyfi,finite-fault,general-text,impact-link,im...  None   0.356  0.73  39   \n",
      "13  ,dyfi,impact-text,losspager,moment-tensor,orig...  None   2.029  1.07  43   \n",
      "14        ,dyfi,losspager,origin,phase-data,shakemap,  None   2.175  0.75  33   \n",
      "15  ,dyfi,impact-link,losspager,moment-tensor,orig...  None   4.591  0.75  14   \n",
      "16  ,dyfi,impact-text,losspager,moment-tensor,orig...  None   1.068  0.61  30   \n",
      "17  ,dyfi,impact-link,impact-text,losspager,moment...  None   2.822  0.95  36   \n",
      "18  ,dyfi,finite-fault,general-text,impact-link,im...  None   3.625  1.22   9   \n",
      "19  ,dyfi,general-text,losspager,moment-tensor,ori...  None   0.169  0.55  30   \n",
      "\n",
      "   magType        type                                              title  \n",
      "0      mww  earthquake           M 5.5 - 7 km SW of Heunghae, South Korea  \n",
      "1      mww  earthquake             M 6.5 - 18 km W of Parrita, Costa Rica  \n",
      "2      mww  earthquake                M 6.1 - 7 km NNE of Ixtepec, Mexico  \n",
      "3      mww  earthquake               M 5.1 - 29 km ENE of Koysinceq, Iraq  \n",
      "4      mww  earthquake                   M 6.5 - 168 km SW of Mawu, China  \n",
      "5      mww  earthquake                   M 6.6 - 11 km ENE of Kos, Greece  \n",
      "6      mww  earthquake              M 6.5 - 2 km S of Lim-oo, Philippines  \n",
      "7      mww  earthquake     M 6.8 - 28 km SW of Puerto San José, Guatemala  \n",
      "8      mww  earthquake           M 6.9 - 2 km SSW of San Pablo, Guatemala  \n",
      "9      mww  earthquake                  M 6.3 - 5 km S of Plomári, Greece  \n",
      "10     mww  earthquake                 M 5.6 - 34 km NNW of Bojn?rd, Iran  \n",
      "11     mww  earthquake          M 5.4 - 131 km ESE of Murghob, Tajikistan  \n",
      "12     mww  earthquake               M 6.9 - 40 km W of Valparaíso, Chile  \n",
      "13     mww  earthquake            M 6.1 - 61 km NNW of Torbat-e J?m, Iran  \n",
      "14      mb  earthquake      M 5.2 - 7 km SSE of Stilfontein, South Africa  \n",
      "15     mww  earthquake  M 6.6 - 81 km NNE of Ust’-Kamchatsk Staryy, Ru...  \n",
      "16     mww  earthquake                  M 5.6 - 5 km NW of Samsat, Turkey  \n",
      "17     mww  earthquake              M 6.5 - 11 km N of Mabua, Philippines  \n",
      "18     mww  earthquake     M 7.9 - 35 km WNW of Panguna, Papua New Guinea  \n",
      "19     mww  earthquake               M 5.6 - 4 km SE of Cittareale, Italy  \n",
      "\n",
      "[20 rows x 26 columns]\n"
     ]
    }
   ],
   "source": [
    "import pandas as pd\n",
    "import requests\n",
    "import json\n",
    "\n",
    "\n",
    "data = requests.get(\"https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2017-01-01&endtime=2017-12-31&alertlevel=yellow\")\n",
    "data = json.loads(data.text)\n",
    "a = data[\"features\"]\n",
    "df = pd.DataFrame(a)\n",
    "b = df.properties\n",
    "c = pd.json_normalize(b)\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "c52a5d19",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyodbc\n",
    "server = 'OTUSDPSQL'\n",
    "database = 'B12022_Target_JReju'\n",
    "username = 'B12022_JReju'\n",
    "password = 'Tiger123*'\n",
    "cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)\n",
    "cursor = cnxn.cursor()\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "c183eff9",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "cursor.execute('create table api(Place varchar(50), Type varchar(25), Magnitude numeric(3,1), Title varchar(50))')\n",
    "for index,row in c.iterrows():\n",
    "    cursor.execute(\"INSERT INTO dbo.api values(?,?,?,?)\", row['place'],row['type'],row['cdi'],row['title'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "97af1ecf",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<function Cursor.close>"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "cnxn.commit()\n",
    "cursor.close"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "689750bc",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAWoAAAE+CAYAAACkx1oCAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjQuMywgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy/MnkTPAAAACXBIWXMAAAsTAAALEwEAmpwYAAAxmElEQVR4nO2de9xVVbX3vwNEKREvyDHzBpWJiqBImqIo0kWztMxbqa/2Zhy1Olp+vHVe0+gcL0V2uhzleEs7KeY1LS3NvJKGgjyACKgYKlqK10SzEMf7x1wbNpv9PHutvebeewK/7+ezPs+z1l5zrLH3nHOsOcccc05zd4QQQqRLr04rIIQQomdkqIUQInFkqIUQInFkqIUQInFkqIUQInFkqIUQInHWaoXQjTfe2AcNGtQK0UIIsVoybdq0l9x9YL3PWmKoBw0axNSpU1shWgghVkvM7OnuPpPrQwghEkeGWgghEieXoTazb5jZbDN71MwmmVnfVismhBAi0NBHbWabAf8GbOfufzeza4HDgSuKPGjJkiUsXLiQt99+uylFRefo27cvm2++OX369Om0KkKskeQdTFwLeI+ZLQHeCzxf9EELFy5kvfXWY9CgQZhZ0eSiQ7g7L7/8MgsXLmTw4MGdVkeINZKGrg93fw6YADwD/AV43d3vKPqgt99+mwEDBshIr2KYGQMGDFBPSIgO0tBQm9mGwIHAYOD9wLpmdmSd+8aZ2VQzm7po0aLuZJVUV3QC5ZsQnSXPYOLHgD+7+yJ3XwLcCOxee5O7X+zuI9195MCBdWO212juueceHnjggWXnEydO5Oc//3kHNRJCrCrk8VE/A3zUzN4L/B0YC5SezTLo9FvLiliBBeftX1rG0qVL6d27dwRtVuSdd97hnnvuoV+/fuy+e3jHHXfccdGfI4SIQyP7FMPeFCGPj3oKcD3wCDArS3Nxi/WKzoIFCxgyZAhHH300w4YN4+CDD+att95i0KBBjB8/nj322IPrrruOSZMmscMOOzB06FBOO+20Zen79evHySefzIgRIxg7diwV9878+fPZd9992Xnnndlzzz2ZO3cuAMcccwzf/OY3GTNmDIcddhgTJ07khz/8ITvuuCP3338/Z599NhMmTOhRxnXXXcfQoUMZPnw4o0eP7vY7HXvssQwdOpQjjjiCO++8k1GjRrH11lvz0EMPAfDQQw+x++67s9NOO7H77rszb948AN566y0OPfRQhg0bxmGHHcauu+6qGaVCJEiuOGp3P8vdh7j7UHc/yt3/0WrFWsG8efMYN24cM2fOpH///lx44YVACD+bPHkyo0eP5rTTTuOuu+6iq6uLhx9+mF/96lcAvPnmm4wYMYJHHnmEvfbai+985zsAjBs3jp/85CdMmzaNCRMmcMIJJyx73uOPP86dd97JDTfcwHHHHcc3vvENurq62HPPPVfQqzsZ48eP5/bbb2fGjBnccsstdb/Tk08+yYknnsjMmTOZO3cuV199NZMnT2bChAmcc845AAwZMoT77ruP6dOnM378eL71rW8BcOGFF7Lhhhsyc+ZMzjzzTKZNmxbvxxZCRKMla32kyhZbbMGoUaMAOPLII/nxj38MwGGHHQbAww8/zN57703Fx37EEUdw33338dnPfpZevXotu+/II4/koIMOYvHixTzwwAMccsghy57xj38sf4cdcsghDV0pPckYNWoUxxxzDIceeigHHXRQ3fSDBw9mhx12AGD77bdn7NixmBk77LADCxYsAOD111/n6KOP5oknnsDMWLJkCQCTJ0/mxBNPBGDo0KEMGzas0U8ohOgAa5Shro1eqJyvu+66QIgZLiLr3XffZYMNNqCrq6vuPRW5PdGTjIkTJzJlyhRuvfVWdtxxR7q6uhgwYMAK96yzzjrL/u/Vq9ey8169evHOO+8AcOaZZzJmzBhuuukmFixYwN577w0U+75CiM6xRq318cwzz/Dggw8CMGnSJPbYY48VPt9111259957eemll1i6dCmTJk1ir732AoJBvf766wG4+uqr2WOPPejfvz+DBw/muuuuA4LhmzFjRt1nr7feerzxxhsrXe9Jxvz589l1110ZP348G2+8Mc8++2xT3/v1119ns802A+CKK65Ydn2PPfbg2muvBeCxxx5j1qxZTckXQrSWNcpQb7vttlx55ZUMGzaMV155heOPP36FzzfddFPOPfdcxowZw/DhwxkxYgQHHnggEFrHs2fPZuedd+auu+7i29/+NgBXXXUVl112GcOHD2f77bfn5ptvrvvsz3zmM9x0003LBhOr6U7GKaecsmxgc/To0QwfPpznn3+eT33qU4W+96mnnsoZZ5zBqFGjWLp06bLrJ5xwAosWLWLYsGGcf/75DBs2jPXXX7+QbCFE67FWdH9HjhzptdEDc+bMYdttt43+rLwsWLCAT3/60zz66KNNpe/Xrx+LFy+OrFVnWbp0KUuWLKFv377Mnz+fsWPH8vjjj7P22muvdG+n80+IdtKJ8Dwzm+buI+t9tkb5qMWKvPXWW4wZM4YlS5bg7lx00UV1jbQQorOsMYZ60KBBTbemgdWuNQ3Bb664aSHSZ43yUQshxKpIWw21wsFWTZRvQnSWthnqvn378vLLL6vSr2JU1qPu21eb+gjRKdrmo958881ZuHAh3S2BKtKlssOLEKIztM1Q9+nTRzuECCFEE2gwUQghEkeGWgghEkeGWgghEkeGWgghEkeGWgghEifPLuTbmFlX1fE3MzupDboJIYQgR3ieu88DdgQws97Ac8BNrVVLCCFEhaKuj7HAfHd/uhXKCCGEWJmihvpwYFIrFBFCCFGf3IbazNYGDgCu6+bzcWY21cymapq4EELEo0iLej/gEXd/od6H7n6xu49095GVXbyFEEKUp8haH19Abg8RkU5sd5Qq+i1ET+RqUZvZe4GPAze2Vh0hhBC15GpRu/tbwIAW6yKEEKIOa8yeiSIu6qoL0T40hVwIIRJHhloIIRJHhloIIRJHhloIIRJHg4mrGI0G8UADeaJzaJC5NahFLYQQiSNDLYQQibPKuD5Wly5VCt9D7hOxOrM6lm+1qIUQInFkqIUQInFkqIUQInFkqIUQInFWmcFEIWpZHQeNhKiHWtRCCJE4MtRCCJE4MtRCCJE4ebfi2sDMrjezuWY2x8x2a7ViQgghAnkHE38E/M7dDzaztYH3tlCnlqCBJ7E6o/K9etPQUJtZf2A0cAyAu/8T+Gdr1RJCCFEhj+vjA8Ai4GdmNt3MLjWzdVuslxBCiIw8hnotYARwkbvvBLwJnF57k5mNM7OpZjZ10aJFkdUUQog1lzyGeiGw0N2nZOfXEwz3Crj7xe4+0t1HDhw4MKaOQgixRtPQULv7X4FnzWyb7NJY4LGWaiWEEGIZeaM+vg5clUV8PAV8qXUqCSGEqCaXoXb3LmBka1URQghRD81MFEKIxJGhFkKIxJGhFkKIxJGhFkKIxJGhFkKIxJGhFkKIxNFWXGKNRqvOpUejPFlV8iPm91CLWgghEqctLerV5Q0phBCdQC1qIYRIHBlqIYRIHA0mClESufZEq1GLWgghEkeGWgghEkeGWgghEkeGWgghEkeDiUIIQIOiKaMWtRBCJE6uFrWZLQDeAJYC77i7tuUSQog2UcT1McbdX2qZJqsA6hoKITqBXB9CCJE4eQ21A3eY2TQzG1fvBjMbZ2ZTzWzqokWL4mkohBBrOHkN9Sh3HwHsB3zVzEbX3uDuF7v7SHcfOXDgwKhKCiHEmkwuQ+3uz2d/XwRuAnZppVJCCCGW09BQm9m6ZrZe5X/gE8CjrVZMCCFEIE/UxybATWZWuf9qd/9dS7USQgixjIaG2t2fAoa3QRchhBB1UHieEEIkjgy1EEIkjgy1EEIkjgy1EEIkjgy1EEIkjgy1EEIkjgy1EEIkjgy1EEIkjgy1EEIkjgy1EEIkjgy1EEIkjgy1EEIkjgy1EEIkjgy1EEIkjgy1EEIkjgy1EEIkjgy1EEIkTm5DbWa9zWy6mf2mlQoJIYRYkSIt6hOBOa1SRAghRH1yGWoz2xzYH7i0teoIIYSoJW+L+r+AU4F3u7vBzMaZ2VQzm7po0aIYugkhhCCHoTazTwMvuvu0nu5z94vdfaS7jxw4cGA0BYUQYk0nT4t6FHCAmS0ArgH2MbNftFQrIYQQy2hoqN39DHff3N0HAYcDd7n7kS3XTAghBKA4aiGESJ61itzs7vcA97REEyGEEHVRi1oIIRJHhloIIRJHhloIIRJHhloIIRJHhloIIRJHhloIIRJHhloIIRJHhloIIRJHhloIIRJHhloIIRJHhloIIRJHhloIIRJHhloIIRJHhloIIRJHhloIIRJHhloIIRJHhloIIRInzy7kfc3sITObYWazzew77VBMCCFEIM9WXP8A9nH3xWbWB5hsZr919z+1WDchhBDkMNTu7sDi7LRPdngrlRJCCLGcXD5qM+ttZl3Ai8Dv3X1KS7USQgixjFyG2t2XuvuOwObALmY2tPYeMxtnZlPNbOqiRYsiqymEEGsuhaI+3P014B5g3zqfXezuI9195MCBA+NoJ4QQIlfUx0Az2yD7/z3Ax4C5LdZLCCFERp6oj02BK82sN8GwX+vuv2mtWkIIISrkifqYCezUBl2EEELUQTMThRAicWSohRAicWSohRAicWSohRAicWSohRAicWSohRAicWSohRAicWSohRAicWSohRAicWSohRAicWSohRAicWSohRAicWSohRAicWSohRAicWSohRAicWSohRAicWSohRAicfLsmbiFmd1tZnPMbLaZndgOxYQQQgTy7Jn4DnCyuz9iZusB08zs9+7+WIt1E0IIQY4Wtbv/xd0fyf5/A5gDbNZqxYQQQgQK+ajNbBBho9spLdFGCCHESuQ21GbWD7gBOMnd/1bn83FmNtXMpi5atCimjkIIsUaTy1CbWR+Ckb7K3W+sd4+7X+zuI9195MCBA2PqKIQQazR5oj4MuAyY4+4XtF4lIYQQ1eRpUY8CjgL2MbOu7PhUi/USQgiR0TA8z90nA9YGXYQQQtRBMxOFECJxZKiFECJxZKiFECJxZKiFECJxZKiFECJxZKiFECJxZKiFECJxZKiFECJxZKiFECJxZKiFECJxZKiFECJxZKiFECJxZKiFECJxZKiFECJxZKiFECJxZKiFECJxZKiFECJx8uyZeLmZvWhmj7ZDISGEECuSp0V9BbBvi/UQQgjRDQ0NtbvfB7zSBl2EEELUQT5qIYRInGiG2szGmdlUM5u6aNGiWGKFEGKNJ5qhdveL3X2ku48cOHBgLLFCCLHGI9eHEEIkTp7wvEnAg8A2ZrbQzL7cerWEEEJUWKvRDe7+hXYoIoQQoj5yfQghROLIUAshROLIUAshROLIUAshROLIUAshROLIUAshROLIUAshROLIUAshROLIUAshROLIUAshROLIUAshROLIUAshROLIUAshROLIUAshROLIUAshROLIUAshROLIUAshROLIUAshROLkMtRmtq+ZzTOzJ83s9FYrJYQQYjl5NrftDfw3sB+wHfAFM9uu1YoJIYQI5GlR7wI86e5Pufs/gWuAA1urlhBCiArm7j3fYHYwsK+7H5udHwXs6u5fq7lvHDAuO90GmNeD2I2Bl5pVOpKMFHRIRUYKOqQiIwUdUpGRgg6pyGiHDlu5+8B6H6yVQ7jVubaSdXf3i4GLc8jDzKa6+8g897ZKRgo6pCIjBR1SkZGCDqnISEGHVGR0Woc8ro+FwBZV55sDzzfzMCGEEMXJY6gfBrY2s8FmtjZwOHBLa9USQghRoaHrw93fMbOvAbcDvYHL3X12yefmcpG0WEYKOqQiIwUdUpGRgg6pyEhBh1RkdFSHhoOJQgghOotmJgohROLIUAshROLIUAshROLIUIuOYmbr5LkmWouZ/W/298RO6yJWpm2DiWY2Cuhy9zfN7EhgBPAjd3+6gIwPAxcBm7j7UDMbBhzg7v+RM/03gOvcfWETX6EnuRe7+7jGd0Z51oiePnf3RwrI2h/YHuhblX58QX2GEtaAqZbx8wLpH3H3EY2u1Um3FvBl4HPA+wmTsJ4HbgYuc/clBXQYAJwNjMrkTAbGu/vLeWVkcjYDtqIqmsrd7ysoYytga3e/08zeA6zl7m8USL9ReKy/WvC5jxHW87kF2JuaiW7u/koOGb+mzmS4KhkHFNBnPvAn4H7gPnd/LG/aKhmlyreZDQS+AgxixTz9v0V1KUuemYmxuAgYbmbDgVOBy4CfA3sVkHEJcArwPwDuPtPMrgZyGWqgP3C7mb1CWLPkend/IU/CrALU/Qj4VM7n9yR/lrvvkOPWH2R/+wIjgRmZDsOAKcAeOZ83EXgvMAa4FDgYeKigzmcRKvV2wG2Eij6ZkK+N0r4P2Ax4j5ntxHLD0D/TqxH/C7xGMLCVF+/mwNHAL4DD8n0LIJSF+4DPZ+dHAL8EPpZXgJmdnz3zMWBpdtkzuXllfIWwDMNGwAcJ32ciMLZBui2B72X3vRYuWX/gLuB0d1+Q4/ETgd8BHwCmsaKh9ux6IybkuCcv2wG7AnsCE8xsCDDD3T+XJ3GM8k146d8P3MnyPC2EmR0EnA/8C+E3NcKLtH8hQe7elgN4JPv7beDL1dcKyHg4+zu96lpXE7oMA/4TmAvcmTPNUuAp4M9VR+X8nzllHNTN8XlgUcHvcA2wQ9X5UOCKAuln1vztB9xRUIdZBPfZjOx8E+DXOdMeDdwNvJH9rRy3AAflSD+vh88eL/g9ptW5NrWgjHnAOkXLYo2MLmDtmvI9K0e6Bwkvid5V13oTJqf9qaAOF5X5DrEOQiNyN+B04DfZd/yfAuljlO+uCN/jSWDbsnLa2aJ+w8zOAI4ERmfLp/YpKOMlM/sgWfcqWzDqL03o8iLwV+BlwpsuD08BY939mdoPzOzZnDJ+CVxF/e5h3zrXemKIu8+qnLj7o2a2Y4H0f8/+vmVm7yf8FoML6vB3d3/XzN7JWnAvkq/lhbtfCVxpZp939xsKPhfgVTM7BLjB3d8FMLNewCFAoW4/cLeZHQ5cm50fDNxaUMZThPL8j4LpqvmHu//TLDRmM/dOHt/kxu7+y+oL7r4UuMbMvltQh7Mz95oDz3vOHmc1mZvzbJa7gSqtyFxlI+NvhIbABcAlXtANRZzy/Rsz+5S731YwXTUvuPucEumB9ro+DgO+SGhN/zXrrn2/oIyvEmb3DDGz5wit2SPzJjaz4zM9BgLXA1/x/L6v/wI2BFYy1IRuZx5mAhPc/dE6uuXuZmfMMbNLCd18J/wORQrEb8xsA0IePJLJuKSgDlMzGZcQusuLKd69vMfMfkxw2RTxDx9O6FJeaGavEozB+oRW+eF5Hmxmb2TPNOCbBHcKhNboYuCsHDJ+ksl4C+gysz9QZazd/d/y6JJxr5l9i+AO+jhwAvDrHOmmmdmFwJVApdGwBaHXMj3Pg7OX/ETCb/hcdnlzM3sNOMELjH0Q3JrfIJSJplwGwBcIZeIE4Fgze4Dgq/5DzvT1yveleRLWlItvmdk/gcqYh3sxt8VUM/sl8CtWLBc3FpDRnsHErPV8u7sXNUbdyVsX6OUFBlmydOcB17h7Vww9imJmewJPd9MqH+nuUwvI6gscD4zOLt1H6La+3YRe6wB93f31ommrZAwC+rv7zILpfk/Q/RfZpSOAvYuUlWww0Ny97DKUhTGzo3v6POs55JXVizBA+gmCkbgduNQbVNJsDZ4vE9aJ3yxLu5DgRrrM3Ru28s2sC/hXd59Sc/2jBJfD8ALfY4q775r3/gayhhDGPk4C/sXd39OEjNLlu1nM7Gd1LrsXHJBsZ9THLcBRJY3BOgR/7iBWHIXtcSTXzPq7+9+6GRB04G9ZVzGPDlOBnwFXe8GR9QZy1/awMUNLyXoyL7r72xb62McQInAeI3Qx3ykpf4i7zy1w/zR337nmWu7lIM3sYUJ+TCqaH2Y2rOiLpZVkDZC3K2Uxa+Cs4+5vteHZT7j71t189qS7fyiHjEqkzqGEXsmNrNiKLBKRdAOwI8HHe392TMnbEMlcPt+plOfMNfcjd/9SXh2ydAexvLd3v7v/qmD6vs00nmppp+vjbWBW1oJ6s3KxYNfwZuB1QpeqiC/wauDTWbpKl6aafmZ2ibt/K4esw4EvAQ9XGe07GrV6qjGze4BjPBuNN7NdCO6DIq2WWj8gADn8gLcRdu0BOI8QXfArYB/gI0DZ0KM7gC0L3F/WP/wFms+P6Wb2Z2ASwdAXDgGrYGZbA+eycqhiEb/sHwiRJouz8/cQfs/dGzz7VHf/XuZCWomcdey3ZnYrIWKn2n3yfwjRIHn4Qc159cvWCWUsL+cRgg2adZ2sBUwxsy8B7wN+kh25ydxJHyKUD4DjzOzj7v7VAmIeNbMXyMIMgT8201htZ4u6bhexYNfwUXcfGk+rZXJ7A4+6+7YF0vQiGP+LgHeBywlv7Dzxpp8EfgT8mNBV3Q84tmCLYy51/ICNfLtm9pi7b5f9Pw34SNVg3Iw8XdzuDALhBXh0Hh9ejR9wXcJvCCGKZHFBP2BT+WFm04GjCMb+MEIDYhLBPbag4PMnE3zaPwQ+Q3h5mLs39HNXyehy9x0bXauT7jPu/uuydczM9qOO+6ToYJqZ9S5hYCsyDqpz+XVCFMyLOWV8jODjfxUY7e5PFtRhNjC08tLPytgsd9++oJwtCWGGowihvK81ytOVaCZUpFMHYSBxhxLp/5DnWg45wwgVch7B2O4KnEyBcB5C/PESQtTK+5rQYUqTv8HtwD7Z/zcQtv8BGEAWZpdDxhuEeN+j6xwvdaBcNJUf1ISHEnoaFxBalA8U1GFa9ndW1bX7C8r4IzCi6nxn4MEmfo91I/ymTcsgDLhfTIjrtiZl3Aq8kpXRGwhRG7cCTxBcqI3SjwZmA2cQetS/A95fUIcbK/UjO9+K0PMqImNzQkNgIiHE8FbgjMK/R9kMLaDwKOD3wOMsjz9+KmfaWYSIiccy4zYvO59FFifZIH1fwiSCGYTIjY2yYxAwp+D3mEboon6RmrhZ4MacMs7MdN8N+FdCPPf+BfU4jzCivRvBxzyiupL3kG4LQmTEfSxvbdxFiA4Ym/PZdwG7d/PZn5soG8OAA6iKLW9HflAVr1xz3YC9Cn6HPxJ6AzcCXyPMmOw21rsbGR8B5rPcJ/skMLJA+t2yOvJMdj4cuLCgDjFkvIfgp74ReBr4KbBHQRm/JsxArpxvksnbiND7bZT+IWC7qvODgLkFdbiXEM1zT3a8SZj8cguhp5FHxruEiWgHFnl27dFO10dTXfUs7VY9fe4NpqFbWL/gJMJU4+dY7qP+G2EA7aeNdKiS9QF3fyrv/d3I+BFhxtjfs/OtCKP7Hy8g4+46l93dc/kBzWxb4MMEX95CwmSid3tOtSztRoRBr9KDXGZ2OcFQz2a5+8M956h4mfwwsy+6+9XNpK0j6yOE8MgNgO8SZlh+393/VFBOH8Lm0EZ4gffyHFEbWdopBB//Le6+U3atkLswhowaeRsS3HxHuHvvAulWmKmbDXzP8rB0xPSKbj2kX8n9YmYD8tibqvv36ulzd783h4zhhMHI0YSxmyeAe939srx6QHt91FFCdrKM34IVB9By+XbN7OvuXmhAoRs5pdfIiKBDaT9gJD3WBoYQ/M3zvGDkSrXPvGC6b/b0ubtfUFRmJndDYAsvEA2SjXGc5+6nNPPMKjmXV7+gsiiQW9y9xynkVfdPcfddqw1Z3nGHmDKyNHsR/P77Ebbz+6UXmNiUDeRtCVyXXTqY4JI6BfiNu4/pJt2R7v6L7spHs+WiDGbWj2Cs9yTMd3B3H1RERjujPu42s+9TLmTnu4Rwsvksn7GVezTZ3X9i5RcRanoNgepwsKzldBrBL/oo8B8FW6hPmtn1hK3Rck90sbDOyY2EQbO7vMSbOnthTSTkhwGDzexf3f23BcQ8aGbbefGIi/UK3t8tWRTOAYT60AUsMrN73b3Hl0EFd19qZjubmZX5PYHnzOwidz8+e2HcSrFJSM+a2e6AZy/Qf6PYJKgoMrJImi5CJM8p7v5mzynq8lWCu2IPQtm6kjAL1Ql1rzvWzf6WLh9VA94Qpvb3Ad70AgPdWSTSOsADhMlcoxt5AOpSxm9S0N9zd53jroIy5gFrl9DhrOy5LxDCuP5KWJipiIym1xCgavCKEMp0BWFRqh8CPy+ox3qElb0eIKwyNo4w4STPb/g1gk/1OUK39KNN/p5zgQ9VnX+Q4n7A0YTR/ELjDpHL5vTs77GE2FuK6pDl5y2EKJLCvvYqOecTXn4PA58vmHZjwhIFLxCm8/8CGNABGQ3LYRFZhEHVDdtZJrrR5bPAOQXT7FLn2uDCz+70ly/4pW8gzE5qNn3TiwhVyZiS/f0Twee9DvBEzrTTq/7vAvpk/1sZ45QZu+cIgx1XVhvPOvdWvyy2JKxk+AhhgLdoIbyv5txqr+WQ8SShNTuYMKq+FVUj7Q3SjiEMVN2clY3zevruDcrFpoSY5Y9k14oa6p/VOS7PmbZ2ga4uQtREU8a+UwdhoO/bhFmSBvw7YUGl7+c1tNmLYePs/08S3B13EgYlDymgw7GZDt8qqkMD+UUXuVpp4TnqLALW6Gib68PCNN+zKL6mQzXnEiYpPMqK7pO869w2vYhQFU2vIQCsb2afI7ws1vFszWR3dzMr1GXO/KL7E+J1BxFadFcR/GC3EQYK6yat/ONhKvv3gO+Z2TbkXCOjitlmdhuhi+uEBZEersTAer71DJ5x91sKPreyHMAmhIiP9xGiiOYD15nZOe5+XU/paxhPCFuc7O4Pm9kHCIM+ufGCM95q+EzN+XRCN/szhN+1x98xG9z9GmE97ssIIWm7E1wW53iOGZsxZBCM7CzCRJejsv/PBz5O6D0emEPGcF++FMBZwJ7uvsDMNibkdaN8reiwM8Ef3IwOwEqx3L0I3ytXPbUw9X17Qp2vltOf4guwtXUwMcaaDrMJa1HPYnmEAJ5j9DVLfyHhDXs4Ic52MSHWtqlKZgXXELCV5/2f7u4vWFib+SrPOWiUyXqK4Ma5zN0fqPnsx97NbDQzu8Bz+l5z6FD7fapxzxG5keXJBoRwrNyL1lRHBVhYZe5edx+V+Xbv9xZMjOpGjxgGrqwOtxHqRH9g2+z/awnGabi7NzROkWR0ufuOWYTGQnffrPazHDJmA7t5WPKh4tOtTMia7Q0mm8TQoer+6vL9DrCAECXWcMKNmR1IcJUcQHCJVXiDMKHqgXrpupXXRkNdak2H7P573b3HkJkCsgbRxCJCWdpSA5IxMLN+7r648Z1p042xb2jkzWwGMMbdX7Ew8+tad/9o9lnDCp3dF6MlGsPAVfR4jjCjspAekQxkDBkzCWMu6xF+h+FZa3gA4eXZMLrHzA4lDLL/NyFM8UME19Y+wMvufnKrdYiJme3m7g+WldPuqI+ya/5OM7NzCW+oQpEjWQHchTA91gmVs+iSnFiJXU1q5DRt7M1sDPB5M9uC8KZ/ghCHnWuKbCU9IcyxcPoeZFzi7vPzyoBSLoNzCG6weYTwwOMzvQYSJjblIUY3+f3u/qkqA7d3dv1+CyvSFdGjWZdBr6wnsR5h3ZpBVcZp7Zw6xJBxLmGAGcKaMZdmLr3tgO/kEeDu11qY2n8sy+P8dyPMCLy9HTqY2QWECJM/5rm/VTJWkNfqFrXVX9PBydb89WKhLk1N8jCzTwAXEozJsrV2CW/rE9z9jgI6zCLM1pru7sPNbBOCkav1M/Yko66xd/eDc6St9s1+luCbfZywbm9D32zZ9LFkZHJKGfusJfoB4El3fy1Pmpr0qbQiS+lhZl8grJcOIQ+OJ9Sx7QhRLBfn0KG0jExOb4JdeSdzSe0IPOfuzWzw0RRldTCzRYTBy4GEzT4mufv0gjqUlrECtaOLq+NB6EIOqnN9MMWnkD+U/Z1G6O4aMLugjDJbWFWvJbEWYTUuCFPj80ytLZU+oozzCJERRxI2cfg+IdxwOjlG9zMZRljX4yDClO1dIf/aEoRwwA0J0S+vV8oIYd2Tx3LK+AIhlO0FwkvnTsJSCc8B49qoR2/CRriVPBkJbFqwXMaQUTZPSkdtRNBhevZ3a8JyD7MJrfSzgA+3S0b10RbXR3duB8++SQE5zUaOVKZJ1/IcxbcDi7GrSZnok3fNbCMPq8K9n1C5cPdXs9+51eljydjflw8GXkMYDDzFwiSe+2kwut9TL8nM8vaSYnTVJ5nZtSxvwd1M8VZkaT0IPdVdLOyEXqljf82ZNoqMSHlSyh0VSQcHcPcnCMsBfNfMhhFeyrcReuLtkFElraBlL3oQdqt4EvgtIYztUsJKVk8Cnygo6/eEt9Pg7Ph/5NicljA4M50wSPHF7Dgtu1Z4JasquYOAYU2kq0Q6HEcoUNOBn+VMexihS3UHYZWy/bPrAwmbGbQ0fUQZM4CNsv+3pCo+lRw9FCL1kkigFVlWjxh1LJKM0nlCtuJh9ps+V++zNugwvUjetUrGCvJiCmvVD1eVpundogmtk9MJi4f/NPt/u4LPL10h68gcREFjT+gejgQ2aPKZpdJH0qHsC+eJimGrub42wWfdljyNYeDK6hHJOMWQUTpPKOkGiqRDv2bKdGwZ1Uc7XB8x3Q5NR454WEuizA4eMbpUsaJPXiW0wPbJushFXUll05eW4e6/tBBbv8JgoLsvIvR4GnE5YXLNNay4I8nhhFC7hkTK0x8BH/OajQbMbDChi9twM4oIesSoYzFklM4TyruBSuvg7ovLumtjyKimHVEfZxDWpq33w13r7ufmkFEqcsTM1ie4Pz5LaLFB8AvfTFj17LUcOswB9uuuQnqO3WFiRJ+UlZGCDlVyShVkM9uOMKGgdkeSXC/kSHn6BLCt1+w1aWFBo8c8316DpfSIVMdKy8jklMqTTEbZqI2y5SKZOrJMXqsNNcTJvJLPv52w2P2V7v7X7Nr7CCvxjfUc60CnUCFjyEhBh+zeqAW5GSLlaQwjGUOPGAayo/W0So9oLdEmn59EHammLVEfZd0OUDrzBrn7+TU6/RU4z8Lml3mI0a1LoYuagg5Q0mUQo5dEnG7yuVmkxwGEiRkVA3dEAQMXQ4/SdSyCezBGz7WUGyhSuUiljqwgrKWkkHnA02Z2KqFF/UImcxNCi/rZnhJWSKVCRpCRgg5QviBfS+gl7V2nl3QdIZyrRyLlaWkDV1aPSHUshoErnSeU9/nH0CGVOrKMdvioY7gdynb3NyREeRxImFwCIT70FuB8z7FzeCxS6KImokMpl4GZzXP3bYp+FptIBq6sDjHqWAwZpfOkrBsoVrlIoY6sIKsNhrrjmReDFCrk6kaZgmxmdxBmAdbrJX3cc6zKGKklGsPAldIjUh2LISNGnpR9gZfWIUV6teEZT5vZqdmPBYQfzsxOI6fbgeXdiNPM7IvZcRphd9+8oVhDsvQ/NrMfZf8XcehfSwhJ29vdB7j7AGAM8BqN18it6LC+mZ1nZnPN7OXsmJNd26AdMlLQoYK7P+bu57n71939a9n/eVsbhxFia+81s1fN7FXCTtEbESp6HkrnKdn4R8VIQxj/cPfzCLHA7dAjRh2LIaN0nmSG+AjCi3s3wiqCRnAD5Yk8Ka1DSnVkGR4xKLveQQheP58QG/lqdszJrm1UQE7TE1YIsxC7sjRHZsfplWs5Zcxr5rOa+27PdHlf1bX3Zbr8vh0yUtAhu399wnofc4GXs2NOdm2DVpfLiHl6B2GXnE2qrm2S/T4NZ83G0CNGHYtVT1eHI5U6Un20bT3qTmJmjwPbe7ajStX1tQnTlbfOISNGt67jXdQUdMjui+EyGEIYd6iOBLrFc272GylPS49/rE7d9Qh5EsMdVVaHJOpINe1wfZR2O0ToRrxLWDyolk2p2immATG62il0UVPQAUq6DLJnXUPoFj9E2AzWgElmdnpOHWJ01V9199PcfYi7b5gd22bX8g5Sx+iul3XtxainMfKklBsokg6p1JFltGMw8TTCilHXsDwca3PC4MA1WcVsJKNU68vM9iW4S55g+Y+0JWFyxdfc/XcFv1ZTRGp9lZKRgg6ZjFKtyBi9pFiUbcFFeH6MOhZDRoyea9keYwwdkqgjK8hrg6HueOZl9/Vi+YSZSoTBw+6+NMfXqMjoaIVcnYjwwpkLfNLdn665vhVwR96uZYRucmkDV1aPSHUshozSeRLhBR6lXKRGO2YmVtwOT9dcL+J2eNrKT1h5F/hTzuetRE2FrCyitDmhS9WWChlLRgo6eNgH8LTsaIaTgD9YCN1cqZeUR0CkPP0y9Q3cBYTF4ou2ZpvRI0YdiyHjJErmCcENdDrBDVT7As/jBoqhQxJ1ZAVZbWhRl3Y7RGh9DQMuJvxgvwVOywwFZvaQu++SQ4cYLY6Od1FT0KFKTtkXTqleUkKtyFJ6RKpjUdyDMXquZYlQLpKpI8vktSPqo9OZZ2Hb+f8gtKiPBb4EHODu881survvlENGxytkDBkp6JDdG7UgN0OkPI1hJGPoEcO113Ejm+nRaZ9/EnWkmnYtylTK7QClM69fVYWZYGbTgN+Z2VGZrDycRPkuVQpd1BR0gJIugxi9JCLkqbv/zsw+TDkDF0OPunXMzPq5++J2yIjUcy3lBopULlKpI8tox6JMHc+8IMLWd/fXAdz9bjP7PHADIQSqIalUyAgyUtAByhfkC4GzWd5LmmxmB3jYwTzX6mSR8rS0gYulRzc8Rv4ZkmVllM4Tyvv8Y+hwEmnUkWW0w0cdw+1Qtrv/ReApd/9TzfUtgTPd/SuFvlQJUuiiJqJDKZeBmXW5+45V52MIDYKjgAvdfUTe79KN/Nwt0R5kPOPupYxkHj3M7JvdfQT8u7s3bIxEklE6T8q6gWKVixTqyAqy2mCoO5553cjsRXCJ/C3n/TG6VEkTwzgVfF7TBdnMZgCjK72k7Nowsl6Sh4kSZXTLZWRjGLiyepjZ28D3gXfqfPwNd98gx3NiyCidJxFe4K0uFzFe4IVltMNHXdrtQLyQm6sJO38vBaYB65vZBe7+/RzJS3epIrmBWvnCyNXFjaVDybGL8wlrEy9L7+4zzWwsYaf6hjQwsv1y6nEO3Ru4XDN/I+jxCPArd59WR/axeXSIJKN0nkRwA5XWoQHtdCUtox0t6ihuh0hdkS5339HMjgB2JsTwTnP3YXnTVp030zOI4QYqJSNSFzfG94j+wmmilxSjFfkA8PVuDNyz7r5Fq/Uws22Al939pTqfbeLZ3INWy+hGbqE8aSCrqdZsE+UiCVdSNS1vUbv71bXXsh/utSK+4ZKtrwp9zKwPYcGXn7r7EjPLmzZGzyBG9ElZGaVbgBF0gDiDPmV7STFakV8irPxXj5E5ZZTSw93n1UlXMU65DGwMGVXpyuRJT+RuiZbUIUYdiSFjOd6+pQOvBvoTdhGfC/wFOCVn2mGECv0soRW2YdVnDxXQ4euElvhthDfbVsD9OdN+EfhonetbApfklDEDWL/Od3uC0JppuQzgAWDnbj57to3fo6vmfEyW/qPAIwXytCv7ewRwAcHIz8yZdhtg424+2ySvDnXS9gL6F7g/ih5l6lhkGWXy5JvdHCcDr7RJhxh1pLSM6qMtq+dlbOeh6/FZgqHckuA2yEOl9bUD8Dih9fXB7LMiG0VuBFxC2HDgTEJr6J48Cd39al/ZfVO0Z1Dxn1XLnQmMBW5sk4wvsXJIXIW8LcAY38MsLGlZSX838Hngfwkv0LxU95Ju9prIoJ5w93le09U3s15m1t+baEWaWX8zW5fQ8ptnZqe0WY8ydSymjKbzhNAS3RBYr+boR7GWaBkdYtSRGDKWU9SyN3sQYiD7EJYq3Cu7lvcN11Vz3mzr6+Sq49+BB4HLC36P0i2OGnmFWl+tkNEJHYjQQ8nub7qXFDNPKdGCi6VHmToWWUaZnmuUlmiMclEjr6P1tJ0t6onAnwmF8L4stO71npMsI0rry91/UHX8J7A3YTCrCKVbHGVaX7FkdFoHj9NDgRK9pCo63YqMpUeZOhZTRpk8idUSLV0uOl1HqmmnoS7zw8XoatfjvcAHCqZJoULGkJGCDjEK8uKqYynwSYq/fGPkaQwDV1aPGC+tGDKazhOP5waKUS6SqCPQprU+MqrDavoC+xH2ZGuIR4ocMbNZLI9K6E3Y6md83vQZlQo5kzgVsmj0SSwZKegAWUHOQiZvIwuZJIyYN8Tdf1B9bmYTCKsqFiFGnlYMHAQD14viBq6sHk3XsZgyYuRJ2ciRSOUilTrSPkOdQuYBn676/x3gBXevFz7TEylUyBgyUtABIhXkKprpJcXI0xhGspQeMepYJANXSzN5UuoFHkmHVOpIW1vUtbQ987xmCnqTdLxCRpKRgg5QsiBH6iUl0YqMoUcNzdSx0jIi5UmpF3gkHVKpI+0z1ClkXgwSqpBlZaSgA5QvyKV7Sam0IsvqEaOORaqnMXquZVuiMXRIpY60Z+MAWLaAUoWmfjgz+zqhFT0T2J/gmP+Fu+8ZTdGCWNh95iEvsZmqma1DWFv7k52S0SkdzOzkqtO+hAo2x93/b7N6lKWZPO3OwLn7T9ulR6Q6VlpGDMzsrKpTJ7zAe7t7jPU6mqKT9bSdPuoYboco3YgyRGpx1NKRLmoKOrSoNVuIVFqRZfWIUcci1dMYxHYDxaBj9bSTPupmSCHzOl4hY8hIQYduiFEZihLDfRLDwMXorq8WrC4v8Fh1pG2uj1YQoyvSCVLooqagQyYjustArH7EcDE28cwk6gis+oa67Zkn4pKKT1SkhV7gK7JKuT5a1NUWHSQhn6hIC7mBqlilWtRqfQkh1kRWKUMthBBrIu1clEkIIUQTyFALIUTiyFCL1RoLm8/Wu36FmR3cbn2EaAYZarFa4+67d1oHIcqySoXnCVEUM1vs7v0srN71E2AfwmI/7V3NS4gSqEUt1hQ+R9jtewfgK4Ba2mKVQYZarCmMBia5+1J3fx64q9MKCZEXGWqxJqFJA2KVRIZarCncBxxuZr3NbFNgTKcVEiIvGkwUawo3EQYSZwGPA/d2Vh0h8qMp5EIIkThyfQghROLIUAshROLIUAshROLIUAshROLIUAshROLIUAshROLIUAshROLIUAshROL8f/4L1q//24TQAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "na=pd.json_normalize(a)\n",
    "\n",
    "na.plot(x='id',y=['properties.mag'],kind='bar')\n",
    "na.to_csv('data.csv')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "739da6b8",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
