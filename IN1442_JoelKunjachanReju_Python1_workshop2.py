{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "b14b376d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "joel data engineer\n",
      "systech\n",
      "joel is a data engineer\n",
      "joel\n",
      "joel can walk.........\n",
      "\n",
      "sid is a singer\n",
      "sid is playing guitar\n",
      "sid playback\n",
      "sidsriram\n",
      "\n",
      "padmini is a good dancer\n",
      "padmini dancer\n",
      "mohana dance school\n"
     ]
    }
   ],
   "source": [
    "'''1)\tWrite the Python program for the below representation of Class \n",
    "(using inheritance) and create objects.'''\n",
    "\n",
    "class person():\n",
    "    def __init__ (self,name,designation):\n",
    "        self.name=name\n",
    "        self.designation=designation\n",
    "    def display(self):\n",
    "        print(self.name,self.designation)\n",
    "\n",
    "    def learn(self):\n",
    "        print(self.name+\" learn by doing.......\")\n",
    "\n",
    "    def walk(self):\n",
    "        print(self.name+\" can walk.........\\n\")\n",
    "\n",
    "    def eat(self):\n",
    "        print(self.name)\n",
    "    \n",
    "\n",
    "class programmer(person):\n",
    "    def __init__ (self,name,designation,companyname):\n",
    "        person.__init__(self,name,designation)\n",
    "        self.companyname = companyname\n",
    "\n",
    "    def display(self):\n",
    "        person.display(self)\n",
    "        print(self.companyname)\n",
    "\n",
    "    def coding(self):\n",
    "        print(self.name +\" is a data engineer\")\n",
    "\n",
    "class dancer(person):\n",
    "\n",
    "    def __init__ (self,name,designation,groupname):\n",
    "        person.__init__(self,name,designation)\n",
    "        self.groupname = groupname\n",
    "\n",
    "    def display(self):\n",
    "        person.display(self)\n",
    "        print(self.groupname)\n",
    "\n",
    "    def dancing(self):\n",
    "        print(self.name +\" is a good dancer\")\n",
    "\n",
    "\n",
    "\n",
    "class Singer(person):\n",
    "    def __init__ (self,name,designation,Bandname):\n",
    "        person.__init__(self,name,designation)\n",
    "        self.Bandname = Bandname\n",
    "\n",
    "    def display(self):\n",
    "        person.display(self)\n",
    "        print(self.Bandname)\n",
    "\n",
    "    def Singing(self):\n",
    "        print(self.name +\" is a singer\")\n",
    "    def PlayGuitar(self):\n",
    "        print(self.name+\" is playing guitar\")\n",
    "\n",
    "    \n",
    "a=programmer(\"joel\",\"data engineer\",\"systech\")\n",
    "a.display()\n",
    "a.coding()\n",
    "a.eat() \n",
    "a.walk()\n",
    "\n",
    "b=Singer(\"sid\",\"playback\",\"sidsriram\\n\")\n",
    "b.Singing()\n",
    "b.PlayGuitar()\n",
    "b.display()   \n",
    "\n",
    "c=dancer(\"padmini\",\"dancer\",\"mohana dance school\")\n",
    "c.dancing()\n",
    "c.display()  \n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "957a206a",
   "metadata": {},
   "outputs": [],
   "source": [
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6c2c3eb0",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "fcf45847",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b4b2cbd0",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
